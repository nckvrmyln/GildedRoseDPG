package com.gildedrose;

import com.gildedrose.items.*;

import java.util.List;

class GildedRose {
    List<Item> items;

    public GildedRose(List<Item> items) {
        this.items = items;
    }

    public void updateQuality() {
        items.forEach(this::updateQualityItem);
    }

    private void updateQualityItem(Item item) {
        switch (item.name) {
            case "Sulfuras, Hand of Ragnaros" -> new LegendaryItem(item).update();
            case "Aged Brie" -> new AgedBrie(item).update();
            case "Backstage passes to a TAFKAL80ETC concert" -> new BackstagePass(item).update();
            case "Conjured Mana Cake" -> new ConjuredItem(item).update();
            default -> new RegularItem(item).update();
        }
    }
}
