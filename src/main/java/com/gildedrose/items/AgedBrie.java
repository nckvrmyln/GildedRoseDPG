package com.gildedrose.items;

import com.gildedrose.Item;

public class AgedBrie {

    private final Item item;

    public AgedBrie(Item item) {
        this.item = item;
    }

    public void update() {
        updateQuality();
        item.sellIn--;
        if (item.sellIn < 0) {
            updateQuality();
        }
    }

    private void updateQuality() {
        if (item.quality < 50) {
            item.quality++;
        }
    }
}
