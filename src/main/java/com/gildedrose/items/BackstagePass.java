package com.gildedrose.items;

import com.gildedrose.Item;

public class BackstagePass {

    private final Item item;

    public BackstagePass(Item item) {
        this.item = item;
    }

    public void update() {
        updateQuality();
        item.sellIn--;
        if (item.sellIn < 10) {
            updateQuality();
        }
        if (item.sellIn < 5) {
            updateQuality();
        }
        if (item.sellIn < 0) {
            item.quality = 0;
        }
    }

    private void updateQuality() {
        if (item.quality < 50) {
            item.quality++;
        }
    }
}
