package com.gildedrose.items;

import com.gildedrose.Item;

public class ConjuredItem {

    private final Item item;

    public ConjuredItem(Item item) {
        this.item = item;
    }

    public void update() {
        updateQuality();
        item.sellIn--;
        if (item.sellIn < 0) {
            updateQuality();
        }
    }

    private void updateQuality() {
        if (item.quality > 1) {
            item.quality = item.quality - 2;
        } else if (item.quality > 0) {
            item.quality--;
        }
    }
}
