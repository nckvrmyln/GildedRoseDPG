package com.gildedrose.items;

import com.gildedrose.Item;

public class LegendaryItem {

    private final Item item;

    public LegendaryItem(Item item) {
        this.item = item;
    }

    public void update() {
        // Do nothing
    }
}
