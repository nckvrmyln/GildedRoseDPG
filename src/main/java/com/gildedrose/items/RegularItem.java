package com.gildedrose.items;

import com.gildedrose.Item;

public class RegularItem {

    private final Item item;

    public RegularItem(Item item) {
        this.item = item;
    }

    public void update() {
        updateQuality();
        item.sellIn--;
        if (item.sellIn < 0) {
            updateQuality();
        }
    }

    private void updateQuality() {
        if (item.quality > 0) {
            item.quality--;
        }
    }
}
