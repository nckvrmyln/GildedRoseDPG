package com.gildedrose;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class GildedRoseTest {
    @Nested
    class ConjuredItems {
        @Test
        void conjuredItems_decreasesInQualityTwiceAsFast_withSellInNotReached() {
            Item conjuredManaCake = new Item("Conjured Mana Cake", 2, 10);
            GildedRose app = new GildedRose(List.of(conjuredManaCake));

            app.updateQuality();

            assertThat(conjuredManaCake.quality).isEqualTo(8);
            assertThat(conjuredManaCake.sellIn).isEqualTo(1);
        }

        @Test
        void conjuredItems_decreasesInQualityTwiceAsFast_withSellInReached() {
            Item conjuredManaCake = new Item("Conjured Mana Cake", 0, 10);
            GildedRose app = new GildedRose(List.of(conjuredManaCake));

            app.updateQuality();

            assertThat(conjuredManaCake.quality).isEqualTo(6);
            assertThat(conjuredManaCake.sellIn).isEqualTo(-1);
        }

        @Test
        void conjuredItems_qualityNeverBelowZero() {
            Item conjuredManaCake = new Item("Conjured Mana Cake", 2, 1);
            GildedRose app = new GildedRose(List.of(conjuredManaCake));

            app.updateQuality();

            assertThat(conjuredManaCake.quality).isEqualTo(0);
            assertThat(conjuredManaCake.sellIn).isEqualTo(1);
        }
    }
}
