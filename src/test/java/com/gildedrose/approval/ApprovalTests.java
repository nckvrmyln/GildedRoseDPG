package com.gildedrose.approval;

import com.gildedrose.Item;
import com.gildedrose.TexttestFixture;
import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ApprovalTests {

    @Test
    void testOneDay() {
        List<Item> items = TexttestFixture.run(1);
        Approvals.verifyAll("testOneDay", items);
    }

    @Test
    void testTwoDays() {
        List<Item> items = TexttestFixture.run(2);
        Approvals.verifyAll("testTwoDays", items);
    }

    @Test
    void testFiveDays() {
        List<Item> items = TexttestFixture.run(5);
        Approvals.verifyAll("testFiveDays", items);
    }

    @Test
    void testTenDays() {
        List<Item> items = TexttestFixture.run(10);
        Approvals.verifyAll("testTenDays", items);
    }

    @Test
    void testTwentyDays() {
        List<Item> items = TexttestFixture.run(20);
        Approvals.verifyAll("testTwentyDays", items);
    }

    @Test
    void testThirtyDays() {
        List<Item> items = TexttestFixture.run(30);
        Approvals.verifyAll("testThirtyDays", items);
    }

    @Test
    void testSixtyDays() {
        List<Item> items = TexttestFixture.run(60);
        Approvals.verifyAll("testSixtyDays", items);
    }
}
